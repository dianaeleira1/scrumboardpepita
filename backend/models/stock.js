const mongoose = require("mongoose");

const stockSchema = new mongoose.Schema({
    code: String,
    name: String,
    address: String,
    city: String,
    dbStatus: Boolean,
});
const stock = mongoose.model("stock", stockSchema);
module.exports=stock;