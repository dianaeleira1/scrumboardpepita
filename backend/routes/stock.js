const express = require("express");
const router = express.Router();
const StockController = require("../controllers/stock");

// GET POST PUT DELETE
//http://localhost:3001/api/stock/registerStock
router.post("/registerStock", StockController.registerStock);
//http://localhost:3001/api/stock/listStock
router.get("/listStock", StockController.listStock);

module.exports = router;