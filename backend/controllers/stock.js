const Stock = require("../models/stock");

const registerStock = async (req, res) => {
    if(!req.body.code || !req.body.name ||  !req.body.address || !req.body.city) 
        return res.status(401).send("Process failed: Incomplete data.");

    const existingStock = await Stock.findOne({code: req.body.code});
    if (existingStock) return res.status(401).send("Process failed: code already exists.");

    const stock = new Stock({
        code: req.body.code,
        name: req.body.name,
        address: req.body.price,
        city : req.body.description,       
        dbStatus: true
    });

    const result = await stock.save();
    if(!result) return res.status(401).send("Failed to register stock.")
    return res.status(200).send({stock})
};

const listStock=async(req, res) =>{
    const stock = await Stock.find()
    if(!stock) return res.status(401).send("No stock.");
    return res.status("200").send({stock});
};

module.exports={registerStock, listStock}