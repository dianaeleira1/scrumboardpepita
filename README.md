# Validación del proceso

## Productos

Visualización las bases creadas previamente en MongoDB por medio de compass:
![](./assets/img/compass.png)

1. Visualización de la inserción del primer registro:
![](./assets/img/product1.png)

2. Visualización de la inserción del segundo registro:
![](./assets/img/product2.png) 


3. Visualización de la inserción del tercer registro:
![](./assets/img/product3.png) 

4. Visualización de la inserción del cuarto registro:
![](./assets/img/product4.png) 

5. Visualización de la inserción del quinto registro:
![](./assets/img/product5.png) 

6. Visualización de la validación de duplicados para producto:
![](./assets/img/product_exists.png)

7. Visualización de la base de datos con la colección Producto:
![](./assets/img/compass_5_products.png)

8. Visualización de la colección Producto y su contenido:
![](./assets/img/compass_5_products_detail.png)

9. Visualización del resultado del servicio de consulta para Producto:
![](./assets/img/list_products.png)


## Almacenes


1. Visualización de la inserción del primer registro:
![](./assets/img/stock1.png)

2. Visualización de la inserción del segundo registro:
![](./assets/img/stock2.png) 

3. Visualización de la inserción del tercer registro:
![](./assets/img/stock3.png) 

4. Visualización de la inserción del cuarto registro:
![](./assets/img/stock4.png) 

5. Visualización de la inserción del quinto registro:
![](./assets/img/stock5.png) 

6. Visualización de la validación de duplicados para almacén:
![](./assets/img/stock_exists.png)

7. Visualización de la base de datos con la colección Almacén:
![](./assets/img/compass_stock_collection.png)

8. Visualización de la colección Almacén y su contenido:
![](./assets/img/compass_stock_collection_data.png)

9. Visualización del resultado del servicio de consulta para Almacén:
![](./assets/img/list_stocks.png)